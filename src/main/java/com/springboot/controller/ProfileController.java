package com.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springboot.domain.Profile;
import com.springboot.service.ProfileService;

@Controller
public class ProfileController {

	@Autowired
	private ProfileService profileService;

	@GetMapping("/viewProfile")
	public String viewHomePage(Model model) {
		return findPaginated(1, "id", "asc", model);
	}

	@PostMapping("/createProfile")
	public String createProfile(@ModelAttribute("profile") Profile profile) {
		profileService.saveProfile(profile);
		return "redirect:/";
	}

	@GetMapping("/createProfileForm")
	public String createProfileForm(Model model) {
		Profile profile = new Profile();
		model.addAttribute("profile", profile);
		return "createProfile";
	}
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		int pageSize = 5;
		
		Page<Profile> page = profileService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<Profile> listProfile = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listProfile", listProfile);
		return "index";
	}

}
