package com.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.domain.Profile;

public interface ProfilRepository extends JpaRepository<Profile, Long> {

}
