package com.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.domain.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {

}
