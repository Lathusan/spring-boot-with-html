package com.springboot.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_has_product")
@NamedQueries({
    @NamedQuery(name = "InvoiceHasProduct.findAll", query = "SELECT i FROM InvoiceHasProduct i"),
    @NamedQuery(name = "InvoiceHasProduct.findById", query = "SELECT i FROM InvoiceHasProduct i WHERE i.id = :id"),
    @NamedQuery(name = "InvoiceHasProduct.findByQuntaty", query = "SELECT i FROM InvoiceHasProduct i WHERE i.quntaty = :quntaty"),
    @NamedQuery(name = "InvoiceHasProduct.findBySubtotal", query = "SELECT i FROM InvoiceHasProduct i WHERE i.subtotal = :subtotal")})
public class InvoiceHasProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "quntaty")
    private Integer quntaty;
    @Column(name = "subtotal")
    private Double subtotal;
    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    @ManyToOne
    private Invoice invoiceId;
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    @ManyToOne
    private Product productId;

    public InvoiceHasProduct() {
    }

    public InvoiceHasProduct(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuntaty() {
        return quntaty;
    }

    public void setQuntaty(Integer quntaty) {
        this.quntaty = quntaty;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Invoice getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Invoice invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }
    
}