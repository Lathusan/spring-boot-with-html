package com.springboot.service;

import org.springframework.data.domain.Page;

import com.springboot.domain.Profile;

public interface ProfileService {
	
	void saveProfile(Profile profile);
	Page<Profile> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
