package com.springboot.service;

import org.springframework.data.domain.Page;

import com.springboot.domain.Product;
public interface ProductService {

	void saveProduct(Product product);
	Page<Product> findPaginate(int pageNo, int pageSize, String sortField, String sortDirection);
	
}
